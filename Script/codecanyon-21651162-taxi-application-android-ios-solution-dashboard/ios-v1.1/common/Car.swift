//
//  Car.swift
//
//  Copyright (c) Minimalistic apps. All rights reserved.
//

import Foundation
import ObjectMapper

public final class Car: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let id = "id"
    static let title = "title"
    static let media = "media"
  }

  // MARK: Properties
  public var id: Int?
  public var title: String?
  public var media: Media?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    id <- map[SerializationKeys.id]
    title <- map[SerializationKeys.title]
    media <- map[SerializationKeys.media]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = media { dictionary[SerializationKeys.media] = value }
    return dictionary
  }

}
