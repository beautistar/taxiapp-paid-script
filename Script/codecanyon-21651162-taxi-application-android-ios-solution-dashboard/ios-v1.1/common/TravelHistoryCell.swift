import UIKit
import LGButton

class TravelHistoryCell: UICollectionViewCell {
    @IBOutlet weak var pickupLabel: UILabel!
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var destinationLabel: UILabel!
    @IBOutlet weak var finishTimeLabel: UILabel!
    @IBOutlet weak var rootView: LGButton!
    override func prepareForReuse() {
        super.prepareForReuse()
        rootView.gradientStartColor = nil
        rootView.gradientEndColor = nil
        rootView.shadowColor = UIColor.clear
    }
    
}
