import Foundation

public enum ServerResponse: Int {
    case REQUEST_TIMEOUT = 408
    case OK = 200
    case UNKNOWN_ERROR = 666
    case PAYMENT_ERROR = 400
    case NOT_FOUND = 404
    case UPDATE_APP = 410
    case ACCOUNT_DISABLED = 411
    case ACCOUNT_BLOCKED = 412
    case NO_CLOSE_FOUND = 303
    case HAS_PAYMENT_REQUEST = 901
    case NO_SUFFICIENT_AMOUNT = 902
    case NO_SUFFICIENT_CREDIT = 903
    /**
     * The name of the enumeration (as written in case).
     */
    public var name: String {
        get { return String(describing: self) }
    }
    /**
     * The full name of the enumeration
     * (the name of the enum plus dot plus the name as written in case).
     */
    public var errorMessage: String {
        get {
            switch self.rawValue {
            case 408 : return "Server didn't responded. this error could be due to connection problem or an inside problem in server."
            case 200 : return "Your request was done successfully."
            case 666 : return "Unknown Error"
            case 400 : return "An error happened during payment."
            case 404 : return "No Driver found."
            case 410 : return "A mandatory update is found. please update."
            case 411 : return "Your account has been created but needs approval from dashboard."
            case 412 : return "Your account has been blocked by admin."
            case 303 : return "No close driver has been found."
            case 901 : return "You have requested for payment already."
            case 902 : return "The amount is not sufficient to request a payment."
            case 903 : return "Credit is not sufficient to request a payment."
            default:
                return "Unknown Error"
            }
            
        }
    }
    public var description: String {
        get { return String(reflecting: self) }
    }
}
