//
//  DriverSocketManager.swift
//  Driver
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import Foundation
import SocketIO
import ObjectMapper
import GoogleMaps

class DriverSocketManager{
    var socket : SocketIOClient
    var manager: SocketManager
    static let shared = DriverSocketManager()
    init() {
        manager = SocketManager(socketURL: URL(string: AppDelegate.info["ServerAddress"] as! String)!)
        socket = manager.defaultSocket
    }
    
    func connect(token:String,completionHandler:@escaping ()->Void){
        manager = SocketManager(socketURL: URL(string: AppDelegate.info["ServerAddress"] as! String)!,config:[.connectParams(["token":token])])
        socket = manager.defaultSocket
        socket.on(clientEvent: .connect) {data, ack in
            completionHandler()
        }
        socket.on(clientEvent: .disconnect) {data, ack in
            NotificationCenter.default.post(name: .socketDisconnect, object: data)
        }
        socket.on("error") { data, ack in
            NotificationCenter.default.post(name: .socketError, object: data)
        }
        socket.on("requestReceived") { data, ack in
            let request = Request(travel: Travel(JSON: data[0] as! [String:Any])!,distance: data[1] as! Int, fromDriver: data[2] as! Int,cost: data[3] as! Double)
            NotificationCenter.default.post(name: .requestReceived, object: request)
        }
        socket.on("riderAccepted") { data, ack in
            Travel.shared = Travel(JSON: data[0] as! [String:Any])!
            Rider.shared = Rider(JSON: data[1] as! [String:Any])!
            NotificationCenter.default.post(name: .riderAccepted, object: nil)
        }
        socket.on("driverInfoChanged") { data, ack in
            //TODO:Save new Travel Infos.
            AppConfig.shared.user = Driver(JSON: data[0] as! [String:Any])
        }
        socket.on("cancelTravel") { data, ack in
            NotificationCenter.default.post(name: .cancelTravel, object: nil)
        }
        socket.connect()
    }
    
    func changeStatus(turnOnline:Bool,completionHandler:@escaping (ServerResponse)->Void){
        socket.emitWithAck("changeStatus", turnOnline ? "online" : "offline").timingOut(after: 15) { data in
            completionHandler(ServerResponse(rawValue: data[0] as! Int)!)
        }
    }
    
    func locationChanged(latitude:Double,longitude:Double){
        socket.emit("locationChanged", latitude,longitude)
    }
    
    func acceptOrder(travelId:Int,cost:Double){
        socket.emit("driverAccepted", travelId,cost)
    }
    
    func serviceInLocation(){
        socket.emit("buzz")
    }
    
    func callRequest(completionHandler: @escaping (ServerResponse)->Void){
        socket.emitWithAck("callRequest").timingOut(after: 15) { data in
            completionHandler(ServerResponse(rawValue: data[0] as! Int)!)
        }
    }
    
    func startTravel(){
        socket.emit("startTravel")
    }
    
    func cancelService(completionHandler:@escaping (ServerResponse)->Void){
        socket.emitWithAck("cancelTravel").timingOut(after: 15) { data in
            completionHandler(ServerResponse(rawValue: data[0] as! Int)!)
        }
    }

    func finishService(completionHandler:@escaping (ServerResponse,Bool,Double)->Void){
        socket.emitWithAck("finishedTaxi", Travel.shared.costBest!,Travel.shared.durationReal!,Travel.shared.distanceReal!,Travel.shared.log!).timingOut(after: 15) { data in
            completionHandler(ServerResponse(rawValue: data[0] as! Int)!,data[1] as! Bool, data[2] as! Double)
        }
    }
    
    func getTravels(completionHandler:@escaping (_ travels:[Travel])->Void){
        socket.emitWithAck("getTravels").timingOut(after: 15) { data in
            if data.count > 1 {
                let travels = Mapper<Travel>().mapArray(JSONArray: data[1] as! [[String : Any]])
                completionHandler(travels)
            }
        }
    }
    
    func editProfile(profileInfo:String,completionHandler:@escaping (ServerResponse)->Void){
        socket.emitWithAck("editProfile", profileInfo).timingOut(after: 15) { data in
            completionHandler(ServerResponse(rawValue: data[0] as! Int)!)
        }
    }
    
    func changeProfileImage(imageData:Data,completionHandler:@escaping (ServerResponse,String)->Void) {
        socket.emitWithAck("changeProfileImage", imageData).timingOut(after: 30) { data in
            completionHandler(ServerResponse(rawValue: data[0] as! Int)!,data[1] as! String)
        }
    }
    
    func sendTravelInfo(){
        self.socket.emit("travelInfo", Travel.shared.distanceReal!, Travel.shared.durationReal!, Travel.shared.costBest!)
    }
    
    func changeHeaderImage(imageData:Data,completionHandler:@escaping (ServerResponse,String)->Void) {
        socket.emitWithAck("changeHeaderImage", imageData).timingOut(after: 30) { data in
            completionHandler(ServerResponse(rawValue: data[0] as! Int)!,data[1] as! String)
        }
    }
    
    func requestPayment(completionHandler: @escaping (ServerResponse) -> Void) {
        socket.emitWithAck("requestPayment").timingOut(after: 15) { data in
            completionHandler(ServerResponse(rawValue: data[0] as! Int)!)
        }
    }
    
}
