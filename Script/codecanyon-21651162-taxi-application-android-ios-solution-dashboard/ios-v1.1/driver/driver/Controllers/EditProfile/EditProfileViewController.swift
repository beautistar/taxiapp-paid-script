//
//  EditProfileViewController.swift
//  Driver
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import UIKit
import Eureka
import ImageRow

class EditProfileViewController: FormViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        form +++ Section("Media")
            <<< ImageRow(){
                $0.title = "Header Image"
                $0.sourceTypes = .PhotoLibrary
                $0.clearAction = .no
            }
            <<< ImageRow(){
                $0.title = "Profile Picture"
                $0.sourceTypes = .PhotoLibrary
                $0.clearAction = .no
                }.onChange {
                    let data = UIImageJPEGRepresentation($0.value!, 0.5)
                    DriverSocketManager.shared.changeHeaderImage(imageData: data!) { (res, address) in
                        if res != .OK {
                            DialogBuilder.getDialogForMessage(message: res.description).show(self, sender: nil)
                            return
                        }
                        AppConfig.shared.user?.car?.media?.address = address
                        let encodedData = NSKeyedArchiver.archivedData(withRootObject: AppConfig.shared)
                        let defaults:UserDefaults = UserDefaults.standard
                        defaults.set(encodedData, forKey:"settings")
                    }
                }
                .cellUpdate { cell, row in
                    cell.accessoryView?.layer.cornerRadius = 17
                    cell.accessoryView?.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
                }.onChange {
                    let data = UIImageJPEGRepresentation($0.value!, 0.5)
                    DriverSocketManager.shared.changeProfileImage(imageData: data!) { (res, address) in
                        if res != .OK {
                            DialogBuilder.getDialogForMessage(message: res.description).show(self, sender: nil)
                            return
                        }
                        AppConfig.shared.user?.media?.address = address
                        let encodedData = NSKeyedArchiver.archivedData(withRootObject: AppConfig.shared)
                        let defaults:UserDefaults = UserDefaults.standard
                        defaults.set(encodedData, forKey:"settings")
                    }
            }
            +++ Section("Basic Info")
            <<< PhoneRow(){
                $0.title = "Mobile Number"
                $0.disabled = true
                $0.value = String(AppConfig.shared.user!.mobileNumber!)
            }
            <<< EmailRow(){
                $0.title = "E-Mail"
                $0.value = AppConfig.shared.user!.email
            }
            <<< TextRow(){
                $0.title = "Name"
                $0.value = AppConfig.shared.user!.firstName
                $0.placeholder = "First Name"
                }.onChange {
                    if let firstName = $0.value {
                        AppConfig.shared.user!.firstName = firstName as String
                    }
            }
            <<< TextRow(){
                $0.title = " "
                $0.placeholder = "Last Name"
                $0.value = AppConfig.shared.user!.lastName
                }.onChange {
                    if let lastName = $0.value {
                        AppConfig.shared.user!.lastName = lastName as String
                    }
            }
            +++ Section("Additional Info")
            <<< PushRow<String>() {
                $0.title = "Gender"
                $0.selectorTitle = "Select your gender"
                $0.options = ["Male","Female","Unspecified"]
                $0.value = AppConfig.shared.user!.gender    // initially selected
                }.onChange {
                    AppConfig.shared.user!.gender = $0.value! as String
            }
            <<< TextRow(){
                $0.title = "Address"
                $0.value = AppConfig.shared.user!.address
                }.onChange {
                    if let address = $0.value {
                        AppConfig.shared.user!.address = address as String
                    }
        }
    }
    @IBAction func onSaveButtonClicked(_ sender: UIBarButtonItem) {
        DriverSocketManager.shared.editProfile(profileInfo: AppConfig.shared.user!.toJSONString()!) { (serverResponse) in
            if serverResponse != .OK {
                DialogBuilder.getDialogForMessage(message: serverResponse.description).show(self, sender: nil)
                return
            }
            _ = self.navigationController?.popViewController(animated: true)
            let encodedData = NSKeyedArchiver.archivedData(withRootObject: AppConfig.shared)
            let defaults:UserDefaults = UserDefaults.standard
            defaults.set(encodedData, forKey:"settings")
        }
    }
}
