//
//  AboutViewController.swift
//  Driver
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import UIKit
import Eureka

class AboutViewController:FormViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        form +++ Section(header: "Info", footer: "© 2018 Minimalistic Apps All rights reserved.")
            <<< LabelRow(){
                $0.title = "Application Name"
                $0.value = "Taxi"
            }
            <<< LabelRow(){
                $0.title = "Version"
                $0.value = "1.0"
            }
            <<< LabelRow(){
                $0.title = "Website"
                $0.value = "-"
            }
            <<< LabelRow(){
                $0.title = "Support Phone Number"
                $0.value = "-"
        }
    }
}
