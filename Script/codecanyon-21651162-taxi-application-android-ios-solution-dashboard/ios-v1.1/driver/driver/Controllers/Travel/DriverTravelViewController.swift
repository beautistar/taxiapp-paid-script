//
//  DriverTravelViewController.swift
//  Driver
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import UIKit
import GoogleMaps
import PopupDialog
import MTSlideToOpen
import RMPickerViewController

class DriverTravelViewController: UIViewController, CLLocationManagerDelegate, MTSlideToOpenDelegate {
    enum BarButtonMode {
        case notificate,call
    }
    var barButtonMode = BarButtonMode.notificate
    @IBOutlet weak var labelCost: UILabel!
    @IBOutlet weak var labelDistance: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var map:GMSMapView!
    var timer = Timer()
    var isBuzzed = false
    var isTravelStarted = false
    var timeGone: Int = 0
    var timeWait: Int = 0
    var locationManager = CLLocationManager()
    var driverMarker = GMSMarker()
    var riderMarker = GMSMarker()
    var route = [CLLocationCoordinate2D]()
    @IBOutlet weak var slideStart: MTSlideToOpenView!
    @IBOutlet weak var slideCancel: MTSlideToOpenView!
    @IBOutlet weak var slideFinish: MTSlideToOpenView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        slideStart.delegate = self
        slideFinish.delegate = self
        slideCancel.delegate = self
        slideStart.defaultLabelText = "Slide to start"
        slideFinish.defaultLabelText = "Slide to finish"
        slideCancel.defaultLabelText = "Slide To Cancel"
        slideStart.thumnailImageView.image = #imageLiteral(resourceName: "start")
        slideFinish.thumnailImageView.image = #imageLiteral(resourceName: "finish")
        slideCancel.thumnailImageView.image = #imageLiteral(resourceName: "cross")
        NotificationCenter.default.addObserver(self, selector: #selector(self.travelCanceled), name: .cancelTravel, object: nil)
        if let cost = Travel.shared.costBest {
            labelCost.text = String(format: "%.2f $", cost)
        }
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.distanceFilter = 50
        locationManager.activityType = .automotiveNavigation
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        driverMarker.icon = #imageLiteral(resourceName: "marker taxi")
        riderMarker.icon = #imageLiteral(resourceName: "marker pickup")
        riderMarker.position = (Travel.shared.pickupPoint)!
        riderMarker.map = map
        self.navigationItem.hidesBackButton = true
    }
    
    @IBAction func onRightBarButtonClicked(_ sender: UIBarButtonItem) {
        if barButtonMode == .notificate{
            DriverSocketManager.shared.serviceInLocation()
            barButtonMode = .call
            sender.image = #imageLiteral(resourceName: "call")
        } else {
            showContactModeChoose()
        }
    }
    func mtSlideToOpenDelegateDidFinish(_ sender: MTSlideToOpenView) {
        sender.isEnabled = false
        sender.resetStateWithAnimation(true)
        switch sender {
        case slideCancel:
            self.cancelTravel()
            break
        case slideStart:
            self.startTravel()
            break
        case slideFinish:
            if route.count > 2 {
                timer.invalidate()
                LoadingOverlay.shared.showOverlay(view: self.view)
                Travel.shared.log = MapsUtil.encode(items: MapsUtil.simplify(items: route, tolerance: 50))
                DriverSocketManager.shared.finishService() { (response, usedCredit, cost) in
                    LoadingOverlay.shared.hideOverlayView()
                    _ = self.navigationController?.popViewController(animated: true)
                }
            } else {
                let title = "Something happened"
                let message = "Your GPS doesn't recorded enough info so we could determine this travel as finished."
                
                // Create the dialog
                let popup = PopupDialog(title: title, message: message)
                
                // Create buttons
                let buttonOne = CancelButton(title: "OK") {
                    self.slideFinish.isEnabled = true
                    popup.dismiss()
                }
                
                popup.addButtons([buttonOne])
                self.present(popup, animated: true, completion: nil)
            }
            
            break
        default:
            break;
        }
    }
    
    func startTravel() {
        riderMarker.icon = #imageLiteral(resourceName: "marker destination")
        riderMarker.position = (Travel.shared.destinationPoint)!
        let bounds = GMSCoordinateBounds(coordinate: riderMarker.position, coordinate: driverMarker.position)
        let update = GMSCameraUpdate.fit(bounds, withPadding: 60)
        map.animate(with: update)
        slideCancel.isHidden = true
        slideStart.isHidden = true
        slideFinish.isHidden = false
        isTravelStarted = true
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.onEachSecond), userInfo: nil, repeats: true)
        DriverSocketManager.shared.startTravel()
    }
    
    func cancelTravel() {
        let title = "Cancel Travel"
        let message = "Are you sure you want to cancel travel?"
        
        // Create the dialog
        let popup = PopupDialog(title: title, message: message)
        
        // Create buttons
        let buttonOne = CancelButton(title: "CANCEL") {
            self.slideCancel.isEnabled = true
            popup.dismiss()
        }
        
        let buttonTwo = DefaultButton(title: "YES") {
            DriverSocketManager.shared.cancelService() { data in
                _ = self.navigationController?.popViewController(animated: true)
            }
        }
        
        popup.addButtons([buttonOne, buttonTwo])
        self.present(popup, animated: true, completion: nil)
    }
    
    @objc func travelCanceled() {
        let title = "Cancel Travel"
        let message = "Travel has been canceled by your client"
        
        // Create the dialog
        let popup = PopupDialog(title: title, message: message)
        
        
        let buttonOne = DefaultButton(title: "OK") {
            _ = self.navigationController?.popViewController(animated: true)
        }
        
        popup.addButtons([buttonOne])
        self.present(popup, animated: true, completion: nil)
    }
    
    @objc func onEachSecond(){
        timeGone = timeGone + 1
        Travel.shared.cost = calculateCost()
        Travel.shared.durationReal = timeWait + timeGone
        labelTime.text = computeTime(time: timeGone + timeWait)
        labelDistance.text = String(format:"%.1f km", (Double(Travel.shared.distanceReal!) / 1000.0))
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        if route.count > 0 {
            let distance = Int(userLocation.distance(from: CLLocation(latitude: route[route.count - 1].latitude, longitude: route[route.count - 1].longitude)))
            Travel.shared.distanceReal! += distance
        }
        DriverSocketManager.shared.locationChanged(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        
        route.append(userLocation.coordinate)
        
        driverMarker.position = userLocation.coordinate
        if driverMarker.map == nil {
            driverMarker.map = map
        }
        let bounds = GMSCoordinateBounds(coordinate: riderMarker.position, coordinate: driverMarker.position)
        let update = GMSCameraUpdate.fit(bounds, withPadding: 60)
        map.animate(with: update)
        DriverSocketManager.shared.sendTravelInfo()
    }
    
    func computeTime(time:Int)->String{
        if time == 0 {
            return "00:00"
        }
        let sec = time % 60
        let min = time / 60
        return String(format: "%02d':%02d\"", arguments: [min,sec])
    }
    
    func calculateCost() -> Double {
        return Travel.shared.costBest!
        /*let costDistance = (Double(Travel.shared.travelDistance) / 1000) * (TravelType.current?.everyKm)!
         let costTimeGone = (Double(timeGone) / 60) * (TravelType.current?.everyMinuteGone)!
         let costTimeWait = (Double(timeWait) / 60) * (TravelType.current?.everyMinuteWait)!
         let cost = (TravelType.current?.initial)! + costDistance + costTimeGone + costTimeWait
         
         if cost > 0 {
         return 0
         } else {
         return cost
         }*/
    }
    
    func showContactModeChoose(){
        let style = RMActionControllerStyle.white
        let selectAction = RMAction<UIPickerView>(title: "Select", style: .done) { controller in
            if controller.contentView.selectedRow(inComponent: 0) == 0 {
                self.directCall()
            } else {
                self.requestCall()
            }
        }
        
        let cancelAction = RMAction<UIPickerView>(title: "Cancel", style: .cancel) { _ in
            print("Row selection was canceled")
        }
        
        let actionController = RMPickerViewController(style: style, title: "Contact Method", message: "Select either one of these contact methods", select: selectAction, andCancel: cancelAction)!;
        
        actionController.picker.delegate = self;
        actionController.picker.dataSource = self;
        
        if actionController.responds(to: Selector(("popoverPresentationController:"))) && UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad {
            actionController.modalPresentationStyle = UIModalPresentationStyle.popover
        }
        self.present(actionController, animated: true, completion: nil)
    }
    
    func directCall() {
        if let call = Rider.shared?.mobileNumber,
            let url = URL(string: "tel://\(call)"),
            UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    func requestCall() {
        DriverSocketManager.shared.callRequest() { data in
            if data.rawValue != 200 {
                let popup = DialogBuilder.getDialogForResponse(response: data) { result in
                    if result == .RETRY {
                        self.requestCall()
                    }
                }
                self.present(popup, animated: true, completion: nil)
            } else {
                let dialog = DialogBuilder.getDialogForMessage(message: "Call request has been sent. A call to rider will be made soon.")
                self.present(dialog, animated: true, completion: nil)
            }
        }
    }
}
extension DriverTravelViewController:UIPickerViewDelegate,UIPickerViewDataSource{
    
    // MARK: UIPickerView Delegates
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1;
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 2;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if row == 0 {
            return "Direct Call"
        } else {
            return "Request Call"
        }
    }
}
