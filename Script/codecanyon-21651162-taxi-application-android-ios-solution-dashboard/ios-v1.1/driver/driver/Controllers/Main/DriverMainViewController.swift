//
//  DriverMainViewController.swift
//  Driver
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import UIKit
import GoogleMaps
import PopupDialog
import Whisper
import iCarousel

class DriverMainViewController: UIViewController, CLLocationManagerDelegate {
    @IBOutlet weak var buttonStatus: UIBarButtonItem!
    @IBOutlet weak var requestsList: iCarousel!
    @IBOutlet weak var map:GMSMapView!
    
    var requests : Set<Request> = []
    var markerTaxi = GMSMarker()
    var markerPickup = GMSMarker()
    var markerDropOff = GMSMarker()
    var locationManager = CLLocationManager()
    var isOnline = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.onRequestReceived), name: .requestReceived, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onRiderAccepted), name: .riderAccepted, object: nil)
        // Create a GMSCameraPosition that tells the map to display the
        // coordinate -33.86,151.20 at zoom level 6.
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.distanceFilter = 1
        locationManager.activityType = .automotiveNavigation
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        requestsList.dataSource = self
        requestsList.delegate = self
        requestsList.type = .rotary
    }
    
    @IBAction func onDriverStatusClicked(_ sender: UIBarButtonItem) {
        buttonStatus.isEnabled = false
        DriverSocketManager.shared.changeStatus(turnOnline: !isOnline) { res in
            self.buttonStatus.isEnabled = true
            if res != .OK  {
                //TODO:Show Error
                let dialog = DialogBuilder.getDialogForResponse(response: res) { dialogResult in
                    if dialogResult == .RETRY {
                        self.onDriverStatusClicked(self.buttonStatus)
                    }
                }
                self.present(dialog, animated: true, completion: nil)
                return
            }
            self.isOnline = !self.isOnline
            if self.isOnline {
                self.buttonStatus.title = "Online"
                DriverSocketManager.shared.locationChanged(latitude: self.markerTaxi.position.latitude, longitude: self.markerTaxi.position.longitude)
            } else {
                self.buttonStatus.title = "Offline"
            }
            
        }
    }
    
    @IBAction func onMenuClicked(_ sender: Any) {
        NotificationCenter.default.post(name: .menuClicked, object: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        if isOnline {
            DriverSocketManager.shared.locationChanged(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        }
        markerTaxi.position = userLocation.coordinate
        markerTaxi.icon = #imageLiteral(resourceName: "marker taxi")
        markerTaxi.map = map
        map.animate(to: GMSCameraPosition.camera(withTarget: userLocation.coordinate, zoom: 16.0))
    }
    
    @objc func onRequestReceived(_ notification: Notification) {
        if let request = notification.object as? Request {
            requestsList.isHidden = false
            requests.insert(request)
            requestsList.reloadData()
        }
    }
    
    @objc func onRiderAccepted(_ notification: Notification) {
        LoadingOverlay.shared.hideOverlayView()
        self.performSegue(withIdentifier: "startTravel", sender: nil)
    }
}
extension DriverMainViewController: iCarouselDataSource, iCarouselDelegate{
    func numberOfItems(in carousel: iCarousel) -> Int {
        return requests.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        let vc = Bundle.main.loadNibNamed("RequestCard", owner: self, options: nil)?[0] as! RequestCard
        vc.layer.cornerRadius = 10
        vc.layer.shadowOpacity = 0.2
        vc.layer.shadowOffset = CGSize(width: 0, height: 0)
        vc.layer.shadowRadius = 4.0
        let shadowRect: CGRect = vc.bounds;
        vc.layer.shadowPath = UIBezierPath(rect: shadowRect).cgPath
        let request = requests[requests.index(requests.startIndex, offsetBy: index)]
        vc.labelOrigin.text = request.travel!.pickupAddress
        vc.labelDestination.text = request.travel!.destinationAddress
        vc.request = request
        var distanceArr = request.distance?.components(separatedBy: " ")
        vc.labelDistance.text = distanceArr?[0]
        vc.labelDistanceUnit.text = distanceArr?[1]
        let fromDriverArr = request.fromDriver?.components(separatedBy: " ")
        vc.labelFromYou.text = fromDriverArr?[0]
        vc.labelFromYouUnit.text = fromDriverArr?[1]
        DispatchQueue.main.asyncAfter(deadline: .now() + 30) { // change 2 to desired number of seconds
            vc.delegate?.reject(request: request)// Your code with delay
        }
        vc.delegate = self
        return vc
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if (option == .spacing) {
            return value * 1.1
        }
        return value
    }
}
extension DriverMainViewController: DriverRequestCardDelegate {
    func accept(request: Request) {
        LoadingOverlay.shared.showOverlay(view: self.navigationController?.view)
        DispatchQueue.main.asyncAfter(deadline: .now() + 30) { // change 2 to desired number of seconds
            LoadingOverlay.shared.hideOverlayView()
        }
        DriverSocketManager.shared.acceptOrder(travelId: request.travel!.id!,cost: request.cost!)
    }
    
    func reject(request: Request) {
        requests.remove(request)
        //requests.remove(at: requestsList.currentItemIndex)
        requestsList.reloadData()
    }
}
