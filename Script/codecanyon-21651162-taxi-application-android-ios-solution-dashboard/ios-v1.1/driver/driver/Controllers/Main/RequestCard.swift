//
//  RequestCard.swift
//  Driver
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import UIKit

class RequestCard:UIView{
    @IBOutlet weak var labelOrigin: UILabel!
    @IBOutlet weak var labelDestination: UILabel!
    @IBOutlet weak var labelFromYou: UILabel!
    @IBOutlet weak var labelDistance: UILabel!
    @IBOutlet weak var buttonAccept: UIButton!
    @IBOutlet weak var buttonReject: UIButton!
    @IBOutlet weak var labelFromYouUnit: UILabel!
    @IBOutlet weak var labelDistanceUnit: UILabel!
    var request: Request?
    var delegate: DriverRequestCardDelegate?
    
    @IBAction func onAcceptTouched(_ sender: UIButton) {
        delegate?.accept(request: request!)
    }
    @IBAction func onRejectTouched(_ sender: Any) {
        delegate?.reject(request: request!)
    }
}
protocol DriverRequestCardDelegate: AnyObject {
    func accept(request:Request)
    func reject(request:Request)
}
