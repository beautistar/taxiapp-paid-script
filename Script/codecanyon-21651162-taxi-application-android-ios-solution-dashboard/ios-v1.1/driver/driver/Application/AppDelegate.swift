//
//  AppDelegate.swift
//  Driver
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import UIKit
import GoogleMaps
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    static var info : [String:Any] {
        get {
            let path = Bundle.main.path(forResource: "Info", ofType: "plist")!
            return NSDictionary(contentsOfFile: path) as! [String: Any]
        }
    }
    var window: UIWindow?
    override init() {
        let path = Bundle.main.path(forResource: "Info", ofType: "plist")!
        let dic = NSDictionary(contentsOfFile: path) as! [String: Any]
        GMSServices.provideAPIKey(dic["GoogleMapsAPIKey"] as! String)
        
        super.init()
        FirebaseApp.configure()
        // not really needed unless you really need it FIRDatabase.database().persistenceEnabled = true
    }
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        UIApplication.shared.isIdleTimerDisabled = true
        // Override point for customization after application launch.
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}
extension Notification.Name {
    static let menuClicked = Notification.Name("menuClicked")
    static let socketDisconnect = Notification.Name("socketDisconnect")
    static let socketError = Notification.Name("socketError")
    static let riderAccepted = Notification.Name("riderAccepted")
    static let driverInfoChanged = Notification.Name("driverInfoChanged")
    static let cancelTravel = Notification.Name("cancelTravel")
    static let requestReceived = Notification.Name("requestReceived")
}
