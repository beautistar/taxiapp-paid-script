//
//  TravelViewController.swift
//  Rider
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import UIKit
import Cosmos
import GoogleMaps
import PopupDialog
import MTSlideToOpen
import RMPickerViewController

class TravelViewController: UIViewController,MTSlideToOpenDelegate{
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var labelBalance: UILabel!
    @IBOutlet weak var labelCost: UILabel!
    @IBOutlet weak var labelDistance: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    var driverMarker = GMSMarker()
    var riderMarker = GMSMarker()
    var isTravelStarted = false
    var driverLocation  = CLLocationCoordinate2D()
    @IBOutlet weak var slideCancel: MTSlideToOpenView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.onServiceStarted), name: .serviceStarted, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onServiceCanceled), name: .serviceCanceled, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onServiceFinished), name: .serviceFinished, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onTravelInfoReceived), name: .travelInfoReceived, object: nil)
        //mapView.camera = camera
        driverMarker.icon = #imageLiteral(resourceName: "marker taxi")
        riderMarker.icon = #imageLiteral(resourceName: "marker pickup")
        self.navigationItem.hidesBackButton = true
        slideCancel.sliderViewTopDistance = 6
        slideCancel.sliderCornerRadious = 22
        slideCancel.delegate = self
        slideCancel.defaultLabelText = "Slide To Cancel"
        slideCancel.thumnailImageView.image = #imageLiteral(resourceName: "cross")
        if let cost = Travel.shared.costBest {
            labelCost.text = String(format: "%.2f $", cost)
        }
        if let balance = AppConfig.shared.user?.balance {
            labelBalance.text = String(format: "%.2f $", balance)
        }
    }

    @IBAction func onRightBarButtonClicked(_ sender: UIBarButtonItem) {
        if isTravelStarted {
            showReviewDialog()
        } else {
            showContactModeChoose()
        }
    }
    
    func mtSlideToOpenDelegateDidFinish(_ sender: MTSlideToOpenView) {
        RiderSocketManager.shared.cancelService() { serverResponse in
            if serverResponse == ServerResponse.OK {
                let title = "Message"
                let message = "Service Has Been Canceled Successfully."
                
                // Create the dialog
                let popup = PopupDialog(title: title, message: message)
                
                // Create buttons
                let buttonOne = DefaultButton(title: "OK") {
                    _ = self.navigationController?.popViewController(animated: true)
                }
                
                popup.addButtons([buttonOne])
                self.present(popup, animated: true, completion: nil)
            } else {
                let title = "Message"
                let message = "Service cancellation was unsuccessful for some reason. try again."
                
                // Create the dialog
                let popup = PopupDialog(title: title, message: message)
                
                // Create buttons
                let buttonOne = DefaultButton(title: "OK") {
                    
                }
                
                popup.addButtons([buttonOne])
                self.present(popup, animated: true, completion: nil)
            }
        }
    }
    
    @objc func onServiceStarted(_ notification: Notification) {
        isTravelStarted = true
        riderMarker.icon = #imageLiteral(resourceName: "marker destination")
        slideCancel.isHidden = true
        self.navigationItem.title = "Travel started"
        self.navigationItem.rightBarButtonItem?.image = #imageLiteral(resourceName: "favorites")
        updateMarkers()
    }
    
    @objc func onServiceCanceled(_ notification: Notification){
        let title = "Message"
        let message = "Service Has Been Canceled by other party. You won't be charged for this trip."
        
        // Create the dialog
        let popup = PopupDialog(title: title, message: message)
        
        // Create buttons
        let buttonOne = DefaultButton(title: "OK") {
            _ = self.navigationController?.popViewController(animated: true)
        }

        popup.addButtons([buttonOne])
        self.present(popup, animated: true, completion: nil)
    }
    
    @objc func onServiceFinished(_ notification: Notification){
        let title = "Finished!"
        var message = ""
        let obj = notification.object as! [Any]
        if (obj[1] as! Bool) {
            message = String(format: "Travel Has been finished and amount of %.2f$ has been withdrawn from your credit", obj[2] as! Double)
        } else {
            message = String(format: "Travel Has been finished There was not sufficient amount of credit to do so. pay %.2f$ in cash!", obj[2] as! Double)
        }
        // Create the dialog
        let popup = PopupDialog(title: title, message: message)
        
        // Create buttons
        let buttonOne = DefaultButton(title: "OK") {
            _ = self.navigationController?.popViewController(animated: true)
        }
        
        popup.addButtons([buttonOne])
        self.present(popup, animated: true, completion: nil)
    }
    
    @objc func onTravelInfoReceived(_ notification: Notification){
        let obj = notification.object as! [Any]
        Travel.shared.distanceReal = obj[0] as? Int
        Travel.shared.durationReal = obj[1] as? Int
        let lat = obj[3] as! Double
        let lng = obj[4] as! Double
        driverLocation = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        labelTime.text = String(format: "%02d':%02d\"",Travel.shared.durationReal! / 60, Travel.shared.durationReal! % 60)
        labelDistance.text = String(format: "%.1f km", Double(Travel.shared.distanceReal!) / 1000)
        updateMarkers()
    }
    
    func updateMarkers() {
        driverMarker.map = mapView
        riderMarker.map = mapView
        driverMarker.position = driverLocation
        if isTravelStarted {
            riderMarker.position = (Travel.shared.destinationPoint)!
        } else {
            riderMarker.position = (Travel.shared.pickupPoint)!
        }
        let bounds = GMSCoordinateBounds(coordinate: riderMarker.position, coordinate: driverMarker.position)
        let update = GMSCameraUpdate.fit(bounds, withPadding: 60)
        mapView.animate(with: update)
    }
    
    @objc func back(sender: UIBarButtonItem) {
        let title = "Cancel Travel"
        let message = "Are you sure you want to cancel travel?"
        
        // Create the dialog
        let popup = PopupDialog(title: title, message: message)
        
        // Create buttons
        let buttonOne = CancelButton(title: "CANCEL") {
            popup.dismiss()
        }
        
        let buttonTwo = DefaultButton(title: "YES") {
            RiderSocketManager.shared.cancelService() { data in
                _ = self.navigationController?.popViewController(animated: true)
            }
        }
        
        popup.addButtons([buttonOne, buttonTwo])
        self.present(popup, animated: true, completion: nil)
    }
    
    
    func showReviewDialog(animated: Bool = true) {
        // Create a custom view controller
        let ratingVC = RatingViewController(nibName: "RatingViewController", bundle: nil)
        
        // Create the dialog
        let popup = PopupDialog(viewController: ratingVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: true)
        
        // Create first button
        let buttonOne = CancelButton(title: "CANCEL", height: 60) {
            //self.label.text = "You canceled the rating dialog"
        }
        
        // Create second button
        let buttonTwo = DefaultButton(title: "RATE", height: 60) {
            RiderSocketManager.shared.reviewDriver(score: ratingVC.cosmosStarRating.rating * 20, review: ratingVC.commentTextField.text!) { data in
                if data.rawValue != 200 {
                    let popup = DialogBuilder.getDialogForResponse(response: data) { result in
                        if result == .RETRY {
                            self.showReviewDialog()
                        }
                    }
                    self.present(popup, animated: true, completion: nil)
                } else {
                    self.navigationItem.rightBarButtonItem?.isEnabled = false
                }
            }
        }
        
        // Add buttons to dialog
        popup.addButtons([buttonOne, buttonTwo])
        
        // Present dialog
        present(popup, animated: animated, completion: nil)
    }
    
    func showContactModeChoose(){
        let style = RMActionControllerStyle.white
        let selectAction = RMAction<UIPickerView>(title: "Select", style: .done) { controller in
            if controller.contentView.selectedRow(inComponent: 0) == 0 {
                self.directCall()
            } else {
                self.requestCall()
            }
        }
        
        let cancelAction = RMAction<UIPickerView>(title: "Cancel", style: .cancel) { _ in
            print("Row selection was canceled")
        }
        
        let actionController = RMPickerViewController(style: style, title: "Contact Method", message: "Select either one of these contact methods", select: selectAction, andCancel: cancelAction)!;
        
        actionController.picker.delegate = self;
        actionController.picker.dataSource = self;
        
        if actionController.responds(to: Selector(("popoverPresentationController:"))) && UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad {
            actionController.modalPresentationStyle = UIModalPresentationStyle.popover
        }
        self.present(actionController, animated: true, completion: nil)
    }
    
    func directCall() {
        if let call = Rider.shared?.mobileNumber,
            let url = URL(string: "tel://\(call)"),
            UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    func requestCall() {
        RiderSocketManager.shared.callRequest() { data in
            if data.rawValue != 200 {
                let popup = DialogBuilder.getDialogForResponse(response: data) { result in
                    if result == .RETRY {
                        self.requestCall()
                    }
                }
                self.present(popup, animated: true, completion: nil)
            } else {
                let dialog = DialogBuilder.getDialogForMessage(message: "Call request has been sent. A call to driver will be made soon.")
                self.present(dialog, animated: true, completion: nil)
            }
        }
    }
}
extension TravelViewController:UIPickerViewDelegate,UIPickerViewDataSource{
    
    // MARK: UIPickerView Delegates
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1;
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 2;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if row == 0 {
        return "Direct Call"
        } else {
            return "Request Call"
        }
    }
}

