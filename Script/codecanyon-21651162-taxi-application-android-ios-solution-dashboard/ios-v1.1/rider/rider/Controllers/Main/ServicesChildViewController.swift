//
//  ServicesChildViewController.swift
//
//  Copyright © 2018 Minimalistic Apps. All rights reserved.
//

import UIKit
import LGButton
import PopupDialog

class ServicesChildViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {
    public var serviceCategoryIndex: Int?
    public var serviceIndex: Int?
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var buttonSelectService: LGButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        buttonSelectService.isEnabled = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let services = ServiceCategory.lastDownloaded[serviceCategoryIndex!].services else {
            return 0
        }
        return (services.count)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "serviceCell", for: indexPath) as! ServicesListCell
        cell.update(service: (ServiceCategory.lastDownloaded[serviceCategoryIndex!].services?[indexPath.item])!)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        buttonSelectService.isEnabled = true
        buttonSelectService.titleString = "Confirm " + ServiceCategory.lastDownloaded[serviceCategoryIndex!].services![indexPath.item].title!
        serviceIndex = indexPath.item
    }
    @IBAction func onSelectServiceClicked(_ sender: LGButton) {
        NotificationCenter.default.post(name: .startTravel, object: ServiceCategory.lastDownloaded[serviceCategoryIndex!].services![serviceIndex!].id)
        
    }
    
}
