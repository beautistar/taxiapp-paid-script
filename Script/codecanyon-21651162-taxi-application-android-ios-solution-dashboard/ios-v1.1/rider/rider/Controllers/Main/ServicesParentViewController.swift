//
//  ServicesViewController.swift
//
//  Copyright © 2018 Minimalistic Apps. All rights reserved.
//

import UIKit
import Tabman
import Pageboy

class ServicesParentViewController: TabmanViewController,PageboyViewControllerDataSource {
    private var viewControllers = [UIViewController]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfViewControllers(in pageboyViewController: PageboyViewController) -> Int {
        if ServiceCategory.lastDownloaded.count == 0 {
            return 0
        }
        var barItems = [Item]()
        for serviceCategory in ServiceCategory.lastDownloaded {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "servicesChild") as! ServicesChildViewController
            viewControllers.append(viewController)
            if let title = serviceCategory.title {
                barItems.append(Item(title: title))
            } else {
                barItems.append(Item(title: "Nothing!"))
            }
            
        }
        bar.items = barItems
        return ServiceCategory.lastDownloaded.count
    }
    
    func viewController(for pageboyViewController: PageboyViewController, at index: PageboyViewController.PageIndex) -> UIViewController? {
        let viewController:ServicesChildViewController = (self.viewControllers[index] as! ServicesChildViewController);
        viewController.serviceCategoryIndex = index
        return viewController
    }
    
    func defaultPage(for pageboyViewController: PageboyViewController) -> PageboyViewController.Page? {
        return nil
    }
}
