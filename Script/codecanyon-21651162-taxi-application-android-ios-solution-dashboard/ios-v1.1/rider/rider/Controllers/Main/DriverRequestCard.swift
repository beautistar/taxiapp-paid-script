//
//  DriverRequestCard.swift
//  Rider
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import UIKit
import Koloda

class DriverRequestCard:UIView{
    @IBOutlet weak var imageHeader: UIImageView!
    @IBOutlet weak var imageDriver: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelCar: UILabel!
    @IBOutlet weak var labelDistance: UILabel!
    @IBOutlet weak var labelDuration: UILabel!
    @IBOutlet weak var labelRating: UILabel!
    @IBOutlet weak var labelCost: UILabel!
    @IBOutlet weak var buttonAccept: UIButton!
    public var driverId: Int = 0
}
