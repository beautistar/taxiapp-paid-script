//
//  SplashViewController.swift
//  Rider
//
//  Copyright © 2018 Minimalistic Apps. All rights reserved.
//

import UIKit
import ObjectMapper
import Firebase
import FirebaseUI

class SplashViewController: UIViewController {
    let defaults:UserDefaults = UserDefaults.standard
    @IBOutlet weak var indicatorLoading: UIActivityIndicatorView!
    @IBOutlet weak var textLoading: UILabel!
    @IBOutlet weak var buttonLogin: UIButton!
    override func viewDidLoad() {
        
    }
    func connectSocket(token:String){
        RiderSocketManager.shared.connect(token: token, completionHandler: {
            self.performSegue(withIdentifier: "segueShowHost", sender: nil)
        })
    }
    
    @IBAction func onLoginClicked(_ sender: UIButton) {
        
        if (AppDelegate.info["TestModeEnabled"] as! Bool) {
            tryLogin(phoneNumber: (AppDelegate.info["TestModeNumber"] as! String!))
        }
        else {
            let auth = FUIAuth.defaultAuthUI()
            auth?.delegate = self
            let phoneAuth = FUIPhoneAuth(authUI: auth!)
            auth?.providers = [phoneAuth]
            phoneAuth.signIn(withPresenting: self, phoneNumber: nil)
        }
    }
    
    
    func tryLogin(phoneNumber:String) {
        var request = URLRequest(url: URL(string: (AppDelegate.info["ServerAddress"] as! String) + "rider_login/")!)
        request.httpMethod = "POST"
        let postString = "user_name=" + phoneNumber
        request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else { // check for fundamental networking error
                return
            }
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 { // check for http errors
                return
            }
            let json = try? JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
            let token = json!["token"] as! String
            AppConfig.shared.token = token
            AppConfig.shared.user = Rider(JSON: json!["user"] as! [String:Any])
            let encodedData = NSKeyedArchiver.archivedData(withRootObject: AppConfig.shared)
            self.defaults.set(encodedData, forKey:"settings")
            self.connectSocket(token: token)
        }
        task.resume()
    }
    override func viewDidAppear(_ animated: Bool) {
        if let data = UserDefaults.standard.data(forKey: "settings"),
            let settings = NSKeyedUnarchiver.unarchiveObject(with: data) as? AppConfig {
            connectSocket(token: settings.token!)
            AppConfig.shared = settings
        } else {
            indicatorLoading.isHidden = true
            textLoading.isHidden = true
            buttonLogin.isHidden = false
        }
    }
}
extension SplashViewController: FUIAuthDelegate{
    func authUI(_ authUI: FUIAuth, didSignInWith user: User?, error: Error?) {
        if(user == nil){
            return
        }
        tryLogin(phoneNumber: (user?.phoneNumber)!)
    }
}
