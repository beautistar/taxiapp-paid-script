//
//  RequestTaxiResultEvent.swift
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import Foundation

class RequestTaxiResultEvent: BaseResultEvent {
    var driversSentTo:Int
    init(code:Int, driversSentTo:Int) {
        self.driversSentTo = driversSentTo
        super.init(code: code)
    }
}
