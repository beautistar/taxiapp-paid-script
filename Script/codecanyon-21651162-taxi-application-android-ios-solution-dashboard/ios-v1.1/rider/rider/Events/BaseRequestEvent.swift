//
//  BaseRequestEvent.swift
//  Rider
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import Foundation
class BaseRequestEvent{
    var resultEvent: BaseResultEvent
    
    init(resultEvent:BaseResultEvent) {
        self.resultEvent = resultEvent
    }
}
