//
//  RequestTaxiEvent.swift
//  Rider
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import Foundation

class RequestTaxiEvent: BaseRequestEvent{
    init(){
        super.init(resultEvent: RequestTaxiResultEvent(code: ServerResponse.REQUEST_TIMEOUT.rawValue,driversSentTo: 0))
    }
}
