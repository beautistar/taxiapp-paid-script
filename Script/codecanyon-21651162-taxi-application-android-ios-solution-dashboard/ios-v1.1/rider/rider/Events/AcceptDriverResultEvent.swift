//
//  AcceptDriverResultEvent.swift
//  Rider
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import Foundation

class AcceptDriverResultEvent: BaseResultEvent {
    override init(code: Int) {
        super.init(code: code)
    }
}
