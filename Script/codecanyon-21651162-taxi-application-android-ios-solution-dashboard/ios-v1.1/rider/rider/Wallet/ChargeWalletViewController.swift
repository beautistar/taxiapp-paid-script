//
//  ChargeWalletViewController.swift
//  Rider
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import UIKit
import Eureka
import Stripe
import CreditCardRow

class ChargeWalletViewController:FormViewController{
    var paymentField: STPPaymentCardTextField?
    @IBAction func onCheckoutClicked(_ sender: Any) {
        let amountRow: StepperRow? = form.rowBy(tag: "amount")
        guard let amountRowValue = amountRow?.value else {
            let dialog = DialogBuilder.getDialogForMessage(message: "Please Enter Amount first")
            self.present(dialog, animated: true, completion: nil)
            return
        }
        let amount = Double(amountRowValue)

        guard let cardParams = paymentField?.cardParams else {
            let dialog = DialogBuilder.getDialogForMessage(message: "Please Enter Card Info first.")
            self.present(dialog, animated: true, completion: nil)
            return
        }
        STPAPIClient.shared().createToken(withCard: cardParams) { (token: STPToken?, error: Error?) in
            guard let token = token, error == nil else {
                let dialog = DialogBuilder.getDialogForMessage(message: error.debugDescription)
                self.present(dialog, animated: true, completion: nil)
                return
            }
            RiderSocketManager.shared.chargeAccount(token: token.tokenId, amount: amount) { (response,message) in
                if message == "" {
                    let dialog = DialogBuilder.getDialogForMessage(message: "Payment was succesful.")
                    self.present(dialog, animated: true, completion: nil)
                } else {
                    let dialog =  DialogBuilder.getDialogForMessage(message: response.description)
                    self.present(dialog, animated: true, completion: nil)
                }
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        form +++ Section("Card Info")
            /*<<< CreditCardRow(){ row in
                row.title = "Credit Card Info"
            }*/
            <<< StripeRow(){ row in
                row.tag = "cardNumber"
                //row.title = "Credit Card Info"
                }
                .cellUpdate { cell, row in
                    self.paymentField = cell.paymentField
            }
            +++ Section("Amount")
            <<< StepperRow(){
                $0.tag = "amount"
                $0.title = "The amount of credit"
            }
        
    }
}
