//
//  DialogBuilder.swift
//  common
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import PopupDialog

public class DialogBuilder{
    public enum ButtonOptions{
        case OK_CANCEL, RETRY_CANCEL, OK
    }
    public enum DialogResult{
        case OK, CANCEL, RETRY
    }
    
    public static func getDialogForResponse(response:ServerResponse, result: @escaping (DialogResult)->Void) -> PopupDialog {
        // Prepare the popup assets
        let title = "An Error happened"
        let message = response.description
        
        // Create the dialog
        let popup = PopupDialog(title: title, message: message)
        
        // Create buttons
        let buttonOne = CancelButton(title: "CANCEL") {
            result(.CANCEL)
        }
        
        let buttonTwo = DefaultButton(title: "RETRY") {
            result(.RETRY)
        }

        
        // Add buttons to dialog
        // Alternatively, you can use popup.addButton(buttonOne)
        // to add a single button
        popup.addButtons([buttonOne, buttonTwo])
        
        return popup
    }
    
    public static func getDialogForResponseWithoutRetry(response:ServerResponse, result: @escaping (DialogResult)->Void) -> PopupDialog {
        // Prepare the popup assets
        let title = "An Error happened"
        let message = response.description
        
        // Create the dialog
        let popup = PopupDialog(title: title, message: message)
        
        // Create buttons
        let buttonOne = CancelButton(title: "OK") {
            result(.CANCEL)
        }
        
        // Add buttons to dialog
        // Alternatively, you can use popup.addButton(buttonOne)
        // to add a single button
        popup.addButtons([buttonOne])
        
        return popup
    }
    
    public static func getDialogForResponse(response:[String:Any], result: @escaping (DialogResult)->Void) -> PopupDialog {
        // Prepare the popup assets
        let title = "An Error happened"
        let message:String
        if (response["status"] as! Int) != 666 {
            message = response.description
        } else {
            message = response["error"] as! String
        }
        // Create the dialog
        let popup = PopupDialog(title: title, message: message)
        
        // Create buttons
        let buttonOne = CancelButton(title: "CANCEL") {
            result(.CANCEL)
        }
        
        let buttonTwo = DefaultButton(title: "RETRY") {
            result(.RETRY)
        }
        
        
        // Add buttons to dialog
        // Alternatively, you can use popup.addButton(buttonOne)
        // to add a single button
        popup.addButtons([buttonOne, buttonTwo])
        
        return popup
    }
    
    public static func getDialogForMessage(message:String) -> PopupDialog {
        // Prepare the popup assets
        let title = "Message"
        
        // Create the dialog
        let popup = PopupDialog(title: title, message: message)
        
        // Create buttons
        let buttonOne = DefaultButton(title: "OK") {
            
        }
        
        
        // Add buttons to dialog
        // Alternatively, you can use popup.addButton(buttonOne)
        // to add a single button
        popup.addButtons([buttonOne])
        
        return popup
    }
}
