//
//  ImageHelper.swift
//  common
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import Foundation

public class ImageHelper {
    static public func getArrayOfBytesFromImage(imageData:NSData) -> NSMutableArray
    {
        // the number of elements:
        let count = imageData.length / MemoryLayout<UInt8>.size
        
        // create array of appropriate length:
        var bytes = [UInt8]()
        
        // copy bytes into array
        imageData.getBytes(&bytes, length:count * MemoryLayout<UInt8>.size)
        
        let byteArray:NSMutableArray = NSMutableArray()

        for i in 0 ..< count {
            byteArray.add(NSNumber(value: bytes[i]))
        }
        
        return byteArray
    }
}
