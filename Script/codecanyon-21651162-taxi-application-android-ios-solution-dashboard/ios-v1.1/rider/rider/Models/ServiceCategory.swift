//
//  ServiceCategory.swift
//
//  Copyright (c) Minimalistic apps. All rights reserved.
//

import Foundation
import ObjectMapper

public final class ServiceCategory: Mappable {
    public static var lastDownloaded = [ServiceCategory]()
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let id = "cat_id"
        static let title = "cat_title"
        static let services  = "services"
    }
    
    // MARK: Properties
    public var id: Int?
    public var title: String?
    public var services: [Service]?
    
    init(){
        
    }
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public required init?(map: Map){
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        id <- map[SerializationKeys.id]
        title <- map[SerializationKeys.title]
        services <- map[SerializationKeys.services]
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = title { dictionary[SerializationKeys.title] = value }
        return dictionary
    }
    
}
