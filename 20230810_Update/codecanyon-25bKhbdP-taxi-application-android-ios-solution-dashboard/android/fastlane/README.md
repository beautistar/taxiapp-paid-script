fastlane documentation
----

# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```sh
xcode-select --install
```

For _fastlane_ installation instructions, see [Installing _fastlane_](https://docs.fastlane.tools/#installing-fastlane)

# Available Actions

## Android

### android config_rider

```sh
[bundle exec] fastlane android config_rider
```



### android config_driver

```sh
[bundle exec] fastlane android config_driver
```



### android release_rider

```sh
[bundle exec] fastlane android release_rider
```



### android release_driver

```sh
[bundle exec] fastlane android release_driver
```



### android release

```sh
[bundle exec] fastlane android release
```



----

This README.md is auto-generated and will be re-generated every time [_fastlane_](https://fastlane.tools) is run.

More information about _fastlane_ can be found on [fastlane.tools](https://fastlane.tools).

The documentation of _fastlane_ can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
