package com.innomalist.taxi.common

class Server {
    companion object {
        private const val HostName = "95.217.23.13"
        const val RiderAddress = "http://$HostName:4000/"
        const val DriverAddress = "http://$HostName:4002/"
//        private const val HostName = "10.0.2.2"
//        const val RiderAddress = "http://$HostName:3001/"
//        const val DriverAddress = "http://$HostName:3002/"
    }
}