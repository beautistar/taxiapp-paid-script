package com.innomalist.taxi.driver.activities.main.repository

import com.apollographql.apollo3.api.ApolloResponse
import com.apollographql.apollo3.api.Optional
import com.google.android.gms.maps.model.LatLng
import com.innomalist.taxi.driver.ClientAPI
import com.minimal.taxi.driver.*
import com.minimal.taxi.driver.type.DriverStatus
import com.minimal.taxi.driver.type.OrderStatus
import com.minimal.taxi.driver.type.PointInput
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class MainActivityRepositoryImpl @Inject() constructor(
    private val webService: ClientAPI
) : MainActivityRepository {
    override suspend fun updateDriverStatus(status: DriverStatus, fcmId: String): ApolloResponse<UpdateDriverStatusMutation.Data> {
        return this.webService.getApolloClient().mutation(UpdateDriverStatusMutation(status, Optional.presentIfNotNull(fcmId))).execute()
    }

    override suspend fun updateDriverLocation(location: LatLng): ApolloResponse<UpdateDriverLocationMutation.Data> {
        return this.webService.getApolloClient().mutation(UpdateDriverLocationMutation(PointInput(location.latitude, location.longitude))).execute()
    }

    override suspend fun me(): ApolloResponse<MeQuery.Data> {
        return this.webService.getApolloClient().query(MeQuery()).execute()
    }

    override suspend fun acceptOrder(id: String): ApolloResponse<UpdateOrderStatusMutation.Data> {
        return this.webService.getApolloClient().mutation(UpdateOrderStatusMutation(id, OrderStatus.DriverAccepted, Optional.Absent)).execute()
    }

    override suspend fun getAvailableOrder(): ApolloResponse<AvailableOrdersQuery.Data> {
        return this.webService.getApolloClient().query(AvailableOrdersQuery()).execute()
    }

    override suspend fun observeNewOrder(): Flow<ApolloResponse<OrderCreatedSubscription.Data>> {
        return this.webService.getApolloClient().subscription(OrderCreatedSubscription()).toFlow()
    }

    override suspend fun observeRemoveOrder(): Flow<ApolloResponse<OrderRemovedSubscription.Data>> {
        return this.webService.getApolloClient().subscription(OrderRemovedSubscription()).toFlow()
    }

    override suspend fun observeOrderUpdated(): Flow<ApolloResponse<OrderUpdatedSubscription.Data>> {
        return this.webService.getApolloClient().subscription(OrderUpdatedSubscription()).toFlow()
    }
}