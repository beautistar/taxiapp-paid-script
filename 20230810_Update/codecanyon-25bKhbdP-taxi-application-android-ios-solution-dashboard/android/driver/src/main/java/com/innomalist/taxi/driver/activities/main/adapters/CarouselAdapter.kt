package com.innomalist.taxi.driver.activities.main.adapters

import android.graphics.Rect
import android.media.Image
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.annotation.Px
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.gms.maps.model.LatLng
import com.innomalist.taxi.common.utils.DistanceFormatter
import com.innomalist.taxi.common.utils.LocationHelper
import com.innomalist.taxi.driver.R
import com.innomalist.taxi.driver.databinding.FragmentRequestBinding
import com.minimal.taxi.driver.fragment.AvailableOrder
import java.text.NumberFormat
import java.util.*
import kotlin.math.round

/** Works best with a [LinearLayoutManager] in [LinearLayoutManager.HORIZONTAL] orientation */
class LinearHorizontalSpacingDecoration(@Px private val innerSpacing: Int) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        super.getItemOffsets(outRect, view, parent, state)

        val itemPosition = parent.getChildAdapterPosition(view)

        outRect.left = if (itemPosition == 0) 0 else innerSpacing / 2
        outRect.right = if (itemPosition == state.itemCount - 1) 0 else innerSpacing / 2
    }
}


class BoundsOffsetDecoration : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(outRect: Rect,
                                view: View,
                                parent: RecyclerView,
                                state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)

        val itemPosition = parent.getChildAdapterPosition(view)

        // It is crucial to refer to layoutParams.width
        // (view.width is 0 at this time)!
        val itemWidth = view.layoutParams.width
        val offset = (parent.width - itemWidth) / 2

        if (itemPosition == 0) {
            outRect.left = offset
        } else if (itemPosition == state.itemCount - 1) {
            outRect.right = offset
        }
    }
}

internal class CarouselAdapter(private val images: List<AvailableOrder>, private val currentLocation: LatLng?, private val listener: OnAvailableOrderInteractionListener) :
    RecyclerView.Adapter<CarouselAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = FragmentRequestBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(vh: ViewHolder, position: Int) {
        val image = images[position]
        vh.bind(image, currentLocation , listener)
    }

    override fun getItemCount(): Int = images.size

    class ViewHolder(var binding: FragmentRequestBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(order: AvailableOrder, currentLocation: LatLng?, listener: OnAvailableOrderInteractionListener) {
            binding.request = order
            val format: NumberFormat = NumberFormat.getCurrencyInstance()
            format.currency = Currency.getInstance(order.currency)
            binding.textCost.text = format.format(order.costBest)
            binding.textUserDestinationDistance.text = DistanceFormatter.format(order.distanceBest)
            binding.buttonAccept.setOnClickListener {
                listener.onAccept(order.id)
            }
            binding.buttonDecline.setOnClickListener {
                listener.onDecline(order.id)
            }
            val firstLatLng = LatLng(
                order.points.first().point.lat,
                order.points.first().point.lng
            )
            val distanceDriver = LocationHelper.distFrom(
                firstLatLng, currentLocation ?: firstLatLng
            )
            val gStart = binding.root.findViewById<View>(R.id.guideline_start)
            val lp = gStart.layoutParams as ConstraintLayout.LayoutParams
//            lp.guidePercent = 0.5f
            lp.guidePercent = (distanceDriver.toFloat() / (order.distanceBest + distanceDriver)).toDouble().round(2).toFloat() * 0.75f
            gStart.layoutParams = lp
            binding.textDriverUserDistance.text = DistanceFormatter.format(distanceDriver)
            binding.executePendingBindings()
        }
    }

    interface OnAvailableOrderInteractionListener {
        fun onAccept(id: String)
        fun onDecline(id: String)
    }
}

fun Double.round(decimals: Int): Double {
    var multiplier = 1.0
    repeat(decimals) { multiplier *= 10 }
    return round(this * multiplier) / multiplier
}