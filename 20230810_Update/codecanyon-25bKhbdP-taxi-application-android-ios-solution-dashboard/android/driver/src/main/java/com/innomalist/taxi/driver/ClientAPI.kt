package com.innomalist.taxi.driver

import android.content.Context
import android.os.Looper
import android.util.Log
import com.apollographql.apollo3.ApolloClient
import com.apollographql.apollo3.api.Adapter
import com.apollographql.apollo3.api.CustomScalarAdapters
import com.apollographql.apollo3.api.json.JsonReader
import com.apollographql.apollo3.api.json.JsonWriter
import com.apollographql.apollo3.exception.ApolloWebSocketClosedException
import com.apollographql.apollo3.network.okHttpClient
import com.apollographql.apollo3.network.ws.SubscriptionWsProtocol
import com.innomalist.taxi.common.utils.MyPreferenceManager
import com.innomalist.taxi.driver.Config.Companion.Backend
import com.minimal.taxi.driver.type.Timestamp
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import java.io.IOException
import java.util.*
import javax.inject.Inject

class ClientAPI @Inject constructor(
    private val applicationContext: Context
) {
    fun getApolloClient(): ApolloClient {
        check(Looper.myLooper() == Looper.getMainLooper()) {
            "Only the main thread can get the apolloClient instance"
        }

        val okHttpClientBuilder = OkHttpClient.Builder()
        val token = MyPreferenceManager.getInstance(applicationContext).token
        if (token != null) {
            val auth = JwtAuthenticationInterceptor()
            auth.setJwtToken(token)
            okHttpClientBuilder.addInterceptor(auth)
        }
        val timestampAdapter = object : Adapter<Date> {
            override fun fromJson(
                reader: JsonReader,
                customScalarAdapters: CustomScalarAdapters
            ): Date {
                val map = reader.nextLong()
                return Date(map)
            }

            override fun toJson(
                writer: JsonWriter,
                customScalarAdapters: CustomScalarAdapters,
                value: Date
            ) {
                writer.value(value.toString())
            }
        }
        return ApolloClient.Builder()
            .httpServerUrl("${Backend}graphql")
            .okHttpClient(okHttpClientBuilder.build())
            .webSocketServerUrl("${Backend}graphql")
            .wsProtocol(SubscriptionWsProtocol.Factory(connectionPayload =  { mapOf("authToken" to token) }))
            .webSocketReconnectWhen {
                Log.e("Reconnection", it.message ?: "Unknown")
                // this is called when an error happens on the WebSocket
                it is ApolloWebSocketClosedException && it.code == 1001
            }
            .addCustomScalarAdapter(Timestamp.type, timestampAdapter)
            .build()
    }

    class JwtAuthenticationInterceptor : Interceptor {
        private var jwtToken: String? = null
        fun setJwtToken(jwtToken: String?) {
            this.jwtToken = jwtToken
        }

        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): Response {
            val original: Request = chain.request()
            val builder: Request.Builder = original.newBuilder()
                .header("Authorization", "Bearer $jwtToken")
            val request: Request = builder.build()
            return chain.proceed(request)
        }
    }
}